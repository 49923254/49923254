#coding: utf-8

# 第一層標題
m1 = "<a href='/about'>有關本站</a>"
m11 = "<a href='/'>Brython程式區</a>"
m111 = "<a href=''>四連桿機構</a>"
m112 = "<a href=''>零件體積表列</a>"
m12 = "<a href='/creoParts'>Creo 零件</a>"
# 第一層標題
m2 = "<a href=''>期末考</a>"
m21 = "<a href=''>Solvespace</a>"
m211 = "<a href='/final_1'>cadp_final_1</a>"
m22 = "<a href=''>Creo 零件區</a>"
m221 = "<a href='/final_2'>cadp_final_2</a>"
m222 = "<a href='/final_3'>cadp_final_3</a>"
m223 = "<a href='/final_4'>cadp_final_4</a>"
# 第一層標題
m3 = "<a href=''>參考資料</a>"
m31 = "<a href=''>Creo</a>"
m311 = "<a href=''>零件繪圖</a>"
m312 = "<a href=''>零件組立</a>"
m313 = "<a href=''>Pro/Web.Link</a>"
m32 = "<a href=''>Solid Edge</a>"
m33 = "<a href=''>Solvespace</a>"
m34 = "<a href=''>Python</a>"
# 第一層標題
m4 = "<a href=''>組員介紹</a>"
m41 = "<a href='/introMember1'>何宗哲</a>"
m42 = "<a href='/introMember2'>卓家賢</a>"
m43 = "<a href='/introMember3'>潘亞程</a>"
m44 = "<a href='/introMember4'>鄭軒</a>"
m45 = "<a href='/introMember5'>陳昶欣</a>"
m46 = "<a href='/introMember6'>陳奕捷</a>"

# 第一層標題
m5 = "<a href=''>W13TEST</a>"
m51 = "<a href='/Solvespaceparts'>Solvespace 零件繪圖</a>"
m52 = "<a href='/Solvespaceasm'>Solvespace 組立操作</a>"
m53 = "<a href='/Creoparts'>Creo 零件繪圖</a>"
m54 = "<a href='/Creoasm'>Creo 組立操作</a>"
m55 = "<a href='/VREP'>VREP 步驟</a>"
m56 = "<a href='/Brython'>Brython 程式計算</a>"
m57 = "<a href=''>Pro/Web.Link </a>"



# 請注意, 數列第一元素為主表單, 隨後則為其子表單
表單數列 = [ \
        [m1, [m11, m111, m112], m12],  \
        [m2, [m21, m211], [m22, m221, m222, m223]], \
        [m3, [m31, m311, m312, m313], m32, m33, m34], \
        [m4, m41, m42, m43, m44,m45,m46],[m5,m51,m52,m53,m54,m55,m56,m57]
        ]

def SequenceToUnorderedList(seq, level=0):
    # 只讓最外圍 ul 標註 class = 'nav'
    if level == 0:
        html_code = "<ul class='nav'>"
    # 子表單不加上 class
    else:
        html_code = "<ul>"
    for item in seq:
        if type(item) == type([]):
            level += 1
            html_code +="<li>"+ item[0]
            # 子表單 level 不為 0
            html_code += SequenceToUnorderedList(item[1:], 1)
            html_code += "</li>"
        else:
            html_code += "<li>%s</li>" % item
    html_code += "</ul>\n"
    return html_code

def GenerateMenu():
    return SequenceToUnorderedList(表單數列, 0) \
+'''
<script type="text/javascript" src="/static/jscript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/static/jscript/dropdown.js"></script>
'''
